# Abiturients Accouting System: Frontend

This is Git-repository of Abiturient Accouting System (AAS) Frontend part. AAS is an web-application for universities to account abiturients (entrants) during the admission campaign, handle the admission's routine work and so on.

This repository includes AAS Frontend source code ([src/](src/)) and public data to serve client needs ([public/](public/))

## Deployment

1. Execute `npm install`
2. Execute `npm run start`
3. Visit [localhost:500](http://localhost:5000 "AAS Website")
