// REST API URL to work with project data
export const APIURL = 'http://api.localhost';

// URL to redirect after user successfully logged in
export const onLoginRedirect = '/';

// URL to redirect after user logged out
export const onLogoutRedirect = '/';